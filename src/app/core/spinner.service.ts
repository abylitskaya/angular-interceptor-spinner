import { Injectable } from '@angular/core';
import { BehaviorSubject, interval, Observable } from 'rxjs';
import { debounceTime, delayWhen, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SpinnerService {
  private _showDelay = 2500;
  private _hideDelay = 100;
  private _spinnerSubject = new BehaviorSubject<boolean>(false);

  constructor() { }

  show(): void {
    this._spinnerSubject.next(true);
  }

  hide(): void {
    this._spinnerSubject.next(false);
  }

  getValue(): Observable<boolean> {
    let intervalApplied = false;
    return this._spinnerSubject
      .asObservable()
      .pipe(
        // add delay for showing spinner
        delayWhen((value) => {
          if (value) {
            // set to true to keep correct order
            intervalApplied = true;
            return interval(this._showDelay);
          }
          return interval(intervalApplied ? this._showDelay : 0);
        }),
        map((value) => {
          if (value) {
            // reset spinner interval
            intervalApplied = false;
          }
          return value;
        }),
        // get last value emitted in n ms (min time spinner is showing)
        debounceTime(this._hideDelay)
      );
  }
}
