import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private apiURL = 'https://reqres.in/api';

  constructor(private http: HttpClient) { }

  sendRequest(delay: number): Promise<any> {
    const url = `${this.apiURL}/users?delay=${delay}`;
    return this.http.get(url).toPromise();
  }

}
