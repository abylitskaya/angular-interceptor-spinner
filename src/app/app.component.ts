import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DataService } from './core/data.service';
import { SpinnerService } from './core/spinner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  title = 'angular-spinner';
  showSpinner: boolean;

  private _destroySubject = new Subject<void>();

  constructor(private dataService: DataService, private spinnerService: SpinnerService) {
    this.spinnerService.getValue()
      .pipe(takeUntil(this._destroySubject))
      .subscribe((value) => {
        this.showSpinner = value;
      });

    this.dataService.sendRequest(29);
    this.dataService.sendRequest(1);
    this.dataService.sendRequest(1);
    this.dataService.sendRequest(1);
    this.dataService.sendRequest(5);
  }

  ngOnDestroy() {
    this._destroySubject.next();
    this._destroySubject.complete();
  }
}
